*Comenzando 🚀

	Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.
	Mira Deployment para conocer como desplegar el proyecto.

*Pre-requisitos 📋

	-JDK 1.8 - http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
	-Eclipse STS - https://spring.io/tools (De acuerdo a su sistema operativo)
	-PostgreSQL 11 o superior (Motor de Base de datos)  https://www.postgresql.org/download/ (De acuerdo a su sistema operativo)
	-pgAdmin IV - (Gestor de PostgreSQL) https://www.pgadmin.org/download/  (De acuerdo a su sistema operativo)

*Instalación 🔧

	-Antes que nada instalar el JDK de java
	-Posteriormente hacer la instalacion del Eclipse STS
	-Por ultimo instalar el PostgreSQL y el pgAdmin para administrar el motor de base de datos en este orden.

*Ejecutando las pruebas ⚙️

	1.- Para probar con los contenedores ya ejecutandose pasamos al punto numero 2; de querer probar desde el SpringToolSuite, vamos a levantar 
		nuestro proyecto, para ellos hacemos lo siguiente:
		-Abrimos el pgAdmin, este abrira una pestaña en nuestro navegador, nos pedira una contraseña, esta fue la que pusimos durante la instalacion.
		-Importamos nuestro proyecto en nuestro workspace.
		-Vamos a apartado de Boot Dashboard en nuestro Eclipse, que por lo regular se encuentra en la parte inferior del lado izquierdo.
		-Seleccionamos el nombre de nuestro proyecto.
		-Damos clic al boton de start/restart(boton con un icono de un cuadro rojo con un triangulo verde).
	
	*NOTA: para los pasos anteriores tendriamos que cambiar en el archivo "application.properties" dbpostgresql por localhost y el password por la 
		configurada en la instalacion de postgres*
	
	2.- Una vez hecho esto podremos probar nuestros servicios con Postman o algun otro programa que sirva para ejecutar servicios REST
		-Dentro de Postman agregamos una nueva pestaña y colocamos alguna de las siguiente URL´s
	
		--Listar-> (GET)http://localhost:8080/organization
			[
			    {
			        "id": "46223def-958a-4788-974c-8e55646ccc08",
			        "name": "Hector",
			        "legalEntity": "Veracruz"
			    },
			    {
			        "id": "5fc2be18-8ef1-48f5-a5ab-dc5917ac3555",
			        "name": "Miguel",
			        "legalEntity": ""
			    },
			    {
			        "id": "7e93838f-c1f6-48bf-a8c8-aa6f64f7ee7b",
			        "name": "Maria",
			        "legalEntity": "Veracruz"
			    }
			]
		--Listar por ID-> (GET)http://localhost:8080/organization/*id a buscar*
			{
			    "id": "46223def-958a-4788-974c-8e55646ccc08",
			    "name": "Hector",
			    "legalEntity": "Veracruz"
			}
		--Registrar-> (POST)http://localhost:8080/organization
			*Ir a la pestaña de "Body", seleccionamos "raw" y donde dice "text" cambiamos por "jSON"*
			{
			    "id":"Ejemplo",
			    "name":"Ejemplo",
			    "legalEntity":"Ejemplo"
			}
		--Actualizar-> (PUT)http://localhost:8080/organization
			*Para actualizar se necesita el ID del registro que se quiera actualizar*
			*Ir a la pestaña de "Body", seleccionamos "raw" y donde dice "text" cambiamos por "jSON"*
			{
			    "id":"46223def-958a-4788-974c-8e55646ccc08",
			    "name":"Hector",
			    "legalEntity":"Mexico"
			}
		--Eliminar-> (DELETE)http://localhost:8080/organization/*id a eliminar*

	*Los pasos anteriores, son para la tabla de Organization, para ChargePoint los pasos son muy similares

	--Listar-> (GET)http://localhost:8080/chargepoint
			[
			    {
			        "id": "aec2d73f-b7ca-4e7f-b98c-9f15f93a3e35",
			        "identity": "Barco"
			    },
			    {
			        "id": "ac8b232f-6530-4573-a370-9a43025bf6bd",
			        "identity": "Avion"
			    }
			]
		--Listar por ID-> (GET)http://localhost:8080/chargepoint/*id a buscar*
			{
			    "id": "aec2d73f-b7ca-4e7f-b98c-9f15f93a3e35",
			    "identity": "Barco"
			}
		--Registrar-> (POST)http://localhost:8080/chargepoint
			*Ir a la pestaña de "Body", seleccionamos "raw" y donde dice "text" cambiamos por "jSON"*
			{
			    "id":"Ejemplo",
			    "identity":"Ejemplo"
			}
		--Actualizar-> (PUT)http://localhost:8080/chargepoint
			*Para actualizar se necesita el ID del registro que se quiera actualizar*
			*Ir a la pestaña de "Body", seleccionamos "raw" y donde dice "text" cambiamos por "jSON"*
			{
			    "id":"aec2d73f-b7ca-4e7f-b98c-9f15f93a3e35",
			    "identity":"Bicicleta"
			}
		--Eliminar-> (DELETE)http://localhost:8080/chargepoint/*id a eliminar*

*Despliegue 📦
	
	Abrimos una consola y en ella colocamos el siguiente comando:
	-docker-compose up --build
	*Si hay conflictos, cerrar todos los procesos de postgres que esten presentes en el administrador de tareas*

*Construido con 🛠️
	
	SpringBoot - El framework web usado
	Maven - Manejador de dependencias