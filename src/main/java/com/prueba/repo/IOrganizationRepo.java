package com.prueba.repo;

import com.prueba.model.Organization;

public interface IOrganizationRepo extends IGenericRepo<Organization, String>{

}
