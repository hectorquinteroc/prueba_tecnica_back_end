package com.prueba.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.prueba.model.Organization;
import com.prueba.repo.IGenericRepo;
import com.prueba.repo.IOrganizationRepo;
import com.prueba.service.IOrganizationService;

@Service
public class OrganizationServiceImpl extends CRUDImpl<Organization, String> implements IOrganizationService{

	@Autowired
	private IOrganizationRepo iOrganizationRepo;
	
	@Override
	protected IGenericRepo<Organization, String> getRepo(){
		return iOrganizationRepo;
	}
	
	@Override
	public Page<Organization> listarPageable(Pageable pageable) {
		return iOrganizationRepo.findAll(pageable);
	}
}
