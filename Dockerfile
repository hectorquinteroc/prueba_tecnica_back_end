FROM maven:3.6.3-jdk-11-slim AS builder
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean package -Dmaven.test.skip=true
RUN ls /app

FROM openjdk:8-jdk-alpine
LABEL maintainer="hquintero@optaresolutions.com"
WORKDIR /workspace
RUN ls /workspace
EXPOSE 8080
COPY --from=builder /app/target/PruebaTecnica-0.0.1-SNAPSHOT.jar PruebaTecnica-0.0.1-SNAPSHOT.jar
ENTRYPOINT exec java -jar /workspace/PruebaTecnica-0.0.1-SNAPSHOT.jar