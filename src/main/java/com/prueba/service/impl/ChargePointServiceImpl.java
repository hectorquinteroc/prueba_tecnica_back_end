package com.prueba.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
//import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.prueba.model.ChargePoint;
import com.prueba.repo.IChargePointRepo;
import com.prueba.repo.IGenericRepo;
import com.prueba.service.IChargePointService;

@Service
public class ChargePointServiceImpl extends CRUDImpl<ChargePoint, String> implements IChargePointService{
	
	@Autowired
	private IChargePointRepo iChargePointRepo;
	
	@Override
	protected IGenericRepo<ChargePoint, String> getRepo(){
		return iChargePointRepo;
	}
	
	@Override
	public Page<ChargePoint> listarPageable(Pageable pageable) {
		return iChargePointRepo.findAll(pageable);
	}
}
