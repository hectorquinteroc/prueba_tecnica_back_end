package com.prueba.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.prueba.model.Organization;

public interface IOrganizationService extends ICRUD<Organization, String>{
	Page<Organization> listarPageable(Pageable pageable);
	
}
