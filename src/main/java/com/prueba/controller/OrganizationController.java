package com.prueba.controller;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.prueba.exception.ModeloNotFoundException;
import com.prueba.model.Organization;
import com.prueba.service.IOrganizationService;

@RestController
@RequestMapping("/organization")
public class OrganizationController {

	@Autowired
	private IOrganizationService iOrganizationService;
	
	@GetMapping
	public ResponseEntity<List<Organization>> listar() throws Exception{
		List<Organization> lista = iOrganizationService.listar();
		return new ResponseEntity<List<Organization>>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Organization> listarPorId(@PathVariable("id") String id) throws Exception{  //@RequestParam
		Organization obj = iOrganizationService.listarPorId(id);
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
		}
		return new ResponseEntity<Organization>(obj, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody Organization paciente) throws Exception{		
		Organization obj = iOrganizationService.registrar(paciente);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<Organization> modificar(@Valid @RequestBody Organization paciente) throws Exception {
		Organization obj = iOrganizationService.modificar(paciente);
		return new ResponseEntity<Organization>(obj, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") String id) throws Exception{
		Organization obj = iOrganizationService.listarPorId(id);
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
		}
		iOrganizationService.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<Organization>> listarPageable(Pageable pageable) throws Exception{
		Page<Organization> pacientes = iOrganizationService.listarPageable(pageable);
		return new ResponseEntity<Page<Organization>>(pacientes, HttpStatus.OK);
	}
}
