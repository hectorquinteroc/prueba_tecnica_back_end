package com.prueba.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.prueba.exception.ModeloNotFoundException;
import com.prueba.model.ChargePoint;
import com.prueba.service.IChargePointService;

@RestController
@RequestMapping("/chargepoint")
public class ChargePointController {

	@Autowired
	private IChargePointService iChargePointService;
	
	@GetMapping
	public ResponseEntity<List<ChargePoint>> listar() throws Exception{
		List<ChargePoint> lista = iChargePointService.listar();
		return new ResponseEntity<List<ChargePoint>>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ChargePoint> listarPorId(@PathVariable("id") String id) throws Exception{  //@RequestParam
		ChargePoint obj = iChargePointService.listarPorId(id);
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
		}
		return new ResponseEntity<ChargePoint>(obj, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody ChargePoint paciente) throws Exception{		
		ChargePoint obj = iChargePointService.registrar(paciente);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<ChargePoint> modificar(@Valid @RequestBody ChargePoint paciente) throws Exception {
		ChargePoint obj = iChargePointService.modificar(paciente);
		return new ResponseEntity<ChargePoint>(obj, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") String id) throws Exception{
		ChargePoint obj = iChargePointService.listarPorId(id);
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
		}
		iChargePointService.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/pageable")
	public ResponseEntity<Page<ChargePoint>> listarPageable(Pageable pageable) throws Exception{
		Page<ChargePoint> pacientes = iChargePointService.listarPageable(pageable);
		return new ResponseEntity<Page<ChargePoint>>(pacientes, HttpStatus.OK);
	}
}
