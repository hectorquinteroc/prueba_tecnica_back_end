package com.prueba.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.prueba.model.ChargePoint;

public interface IChargePointService extends ICRUD<ChargePoint, String>{
	Page<ChargePoint> listarPageable(Pageable pageable);
}
